<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function index(Request $request)
	{
	    $category_id = $request->post('category_id', null);

	    if (empty($category_id)) {
            return response()->json(['error' => 'Обязательно укажите category_id']);
        }

	    $limit = $request->post('limit', 10);
	    $offset = $request->post('offset', 0);

	    // Raw sql
		$sql = "
		    SELECT
                a.first_name
                ,a.last_name
                ,COUNT(fc.film_id) AS cat_film_app
            FROM actor a
            INNER JOIN film_actor fa ON (a.actor_id = fa.actor_id)
            INNER JOIN film_category fc ON (fa.film_id = fc.film_id)
            WHERE fc.category_id = :category_id
            GROUP BY a.actor_id
            ORDER BY cat_film_app DESC, a.actor_id DESC
            LIMIT :limit OFFSET :offset
		";
//		$actors = DB::select($sql, ['category_id' => 2, 'limit' => 10, 'offset' => 0]);

        $actors = DB::table('actor AS a')
            ->join('film_actor AS fa', 'a.actor_id', '=', 'fa.actor_id')
            ->join('film_category AS fc', 'fa.film_id', '=', 'fc.film_id')
            ->where('fc.category_id', $category_id)
            ->groupBy('a.actor_id')
            ->orderBy('cat_film_app', 'DESC')
            ->orderBy('a.actor_id', 'DESC')
            ->select('a.first_name', 'a.last_name', DB::raw('COUNT(fc.film_id) AS cat_film_app'))
            ->skip($offset)
            ->take($limit)
            ->get();

        return response()->json($actors, 200);
	}
}
